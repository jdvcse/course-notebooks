## Jupyter Notebook setup

There are a few additional steps to follow to get jupyter setup on your development VM:

    sudo pip install "ipython<6.0" jupyter
    
[Jupyter contrib extensions](http://jupyter-contrib-nbextensions.readthedocs.io/en/latest/) make working with jupyter notebooks a bit nicer, they can be installed quite easily with the following commands:

    # Jupyter extensions, for TOC display and code folding
    sudo pip install jupyter_contrib_nbextensions
    
    sudo jupyter contrib nbextension install --system
    
Run the notebook server as the al user. Take note of the port it's running on and the token. Or you
can set a password to login, see ``jupyter notebook --help`` for additional options

    cd /opt/al/pkg/assemblyline/docs/notebooks
    sudo -Hu al jupyter notebook
    
Now on your local machine, set up port forwarding and open https://localhost:localport in your browser. You'll need
the token from when you started jupyter notebook remotely.

    # by default, jupyter port is '8888'
    ssh -L <localport>:localhost:<jupyter-port> user@al-dev
    
Once this is done, open up 'Nbextensions' tab on the main page and enable the following extensions:

* Codefolding
* Table of contents